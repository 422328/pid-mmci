# Standard Imports
import argparse
from pathlib import Path
import json
import uuid
import os
import hashlib


# Third-party Imports
import prov.model as prov
import pygit2


# Local Imports
from provenance.constants import GRAPH_NAMESPACE_URI
from provenance.constants import PID_NAMESPACE_URI
from provenance.constants import PROVN_NAMESPACE
from provenance.constants import PROVN_NAMESPACE_URI
from provenance.constants import DEFAULT_NAMESPACE_URI
from provenance.constants import PID_NAMESPACE
from provenance.constants import BUNDLE_PATHOLOGY_DEPARTMENT
from provenance.constants import NAMESPACE_PROV
from provenance.constants import BUNDLE_META
from provenance.constants import OUTPUT_DIR

from provenance.utils import export_to_image
from provenance.utils import export_to_file
from provenance.utils import get_hash


def add_namespaces(prov_obj):
    prov_obj.add_namespace(PROVN_NAMESPACE, PROVN_NAMESPACE_URI)
    prov_obj.set_default_namespace(DEFAULT_NAMESPACE_URI)
    

def get_preproc_provlog(provlog):
    # Get config filepath from provenance log of provenance
    with provlog.open('r') as json_in:
        cfg = json.load(json_in)
    
    # Find out the source of inputs
    cfg_fp = Path(cfg['input']['config'])
    with cfg_fp.open('r') as json_in:
        cfg = json.load(json_in)
    return Path(cfg['configurations']['datagen']['data_sources']['_data']).parent / f'{BUNDLE_PREPROC}.log'


def export_provenance(provn_dir: Path) -> None:
    
    provn_filepath = OUTPUT_DIR / 'provn' / BUNDLE_META
    png_filepath = (OUTPUT_DIR / 'graph' / BUNDLE_META).with_suffix('.png')
    json_filepath = (OUTPUT_DIR / 'json' / BUNDLE_META).with_suffix('.json')
    
    # Provenance of provenance
    output_log = {
        'git_commit_hash': str(pygit2.Repository('.').revparse_single('HEAD').hex),
        'script': str(__file__),
        'eid': str(uuid.uuid4()),
        'input': {},
        'output': {
            'local_png': str(png_filepath),
            'remote_png': str(GRAPH_NAMESPACE_URI + str(Path(BUNDLE_META).with_suffix('.png'))),
            'local_provn': str(provn_filepath),
            'remote_provn': str(PROVN_NAMESPACE_URI + BUNDLE_META)
        }
    }
        
    doc = prov.ProvDocument()
    add_namespaces(doc)
    
    bndl = doc.bundle(f'{PROVN_NAMESPACE}:{BUNDLE_META}')
    add_namespaces(bndl)
    
    module_ns_mapping = {
        BUNDLE_PATHOLOGY_DEPARTMENT: OUTPUT_DIR / 'log' / f'{BUNDLE_PATHOLOGY_DEPARTMENT}.log'
    }
    
    for module, provlog in module_ns_mapping.items():
        output_log['input'][module] = str(provlog.resolve())

        b = bndl.entity(f'{PROVN_NAMESPACE}:{module}', other_attributes={
            f'{NAMESPACE_PROV}:type': bndl.valid_qualified_name(f'{NAMESPACE_PROV}:bundle'),
            'sha256': get_hash((provn_filepath.parent / provlog.stem).with_suffix('.provn'), hash_type='sha256')
        })

        b_gen = bndl.entity(f'{PROVN_NAMESPACE}:{module}_gen', other_attributes={
            f'{NAMESPACE_PROV}:type': bndl.valid_qualified_name(f'{NAMESPACE_PROV}:bundle')
        })

        bndl.specialization(b, b_gen)
        
    
    export_to_image(bndl, png_filepath)
    export_to_file(doc, provn_filepath, format='provn')
    export_to_file(doc, json_filepath, format='json')
    
    with open(OUTPUT_DIR / 'log' / f'{BUNDLE_META}.log', 'w') as json_out:
        json.dump(output_log, json_out, indent=3)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    # Required arguments
    parser.add_argument('--provn_dir', type=Path, required=True, help='Configuration file')
    args = parser.parse_args()
    
    export_provenance(args.provn_dir)
