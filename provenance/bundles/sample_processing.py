# Built-in Imports
import argparse
import json
import uuid
from pathlib import Path
from collections import defaultdict

# Third-party Imports
import prov.model as prov
import pygit2

# Local Imports
from provenance.constants import (
    PROVN_NAMESPACE, 
    PROVN_NAMESPACE_URI,
    DOI_NAMESPACE,
    DOI_NAMESPACE_URI,
    DEFAULT_NAMESPACE_URI,
    NAMESPACE_COMMON_MODEL,
    NAMESPACE_COMMON_MODEL_URI,
    NAMESPACE_DCT,
    NAMESPACE_DCT_URI,
    NAMESPACE_PROV,
    BUNDLE_PATHOLOGY_DEPARTMENT,
    BUNDLE_SURGERY_DEPARTMENT,
    BUNDLE_META,
    OUTPUT_DIR,
    GRAPH_NAMESPACE_URI,
    BACKWARD_PROVN_NAMESPACE_URI,
    BACKWARD_PROVN_NAMESPACE
)
from provenance.utils import export_to_image, export_to_file
from provenance.utils import parse_log


def add_namespaces(prov_object):
    # Declaring namespaces
    prov_object.add_namespace(PROVN_NAMESPACE, PROVN_NAMESPACE_URI)
    prov_object.add_namespace(DOI_NAMESPACE, DOI_NAMESPACE_URI)
    prov_object.add_namespace(NAMESPACE_COMMON_MODEL, NAMESPACE_COMMON_MODEL_URI)
    prov_object.add_namespace(NAMESPACE_DCT, NAMESPACE_DCT_URI)
    prov_object.set_default_namespace(DEFAULT_NAMESPACE_URI)


def export_provenance(nis_log: Path, prov_requests: Path):
    doc = prov.ProvDocument()
    add_namespaces(doc)
    
    # Read provenance requests
    with prov_requests.open('r') as json_in:
        prov_req = json.load(json_in)

    # Creating NIS Sample Processing bundle
    bndl = doc.bundle(f"{PROVN_NAMESPACE}:{BUNDLE_PATHOLOGY_DEPARTMENT}")
    add_namespaces(bndl)
    
    # Sender Agent
    send_agent = bndl.agent(f"surgicalDepartment", other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:senderAgent")
    })
    
    # Backward connector
    entity_identifier = 'sampleConnector'
    doc.add_namespace(BACKWARD_PROVN_NAMESPACE, BACKWARD_PROVN_NAMESPACE_URI)
    bndl.add_namespace(BACKWARD_PROVN_NAMESPACE, BACKWARD_PROVN_NAMESPACE_URI)
    conn_sample_connector = bndl.entity(bndl.valid_qualified_name(f"{DOI_NAMESPACE}:{entity_identifier}"), other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:backwardConnector"),
        f"{NAMESPACE_COMMON_MODEL}:senderBundleId": bndl.valid_qualified_name(f"{BACKWARD_PROVN_NAMESPACE}:{BUNDLE_SURGERY_DEPARTMENT}"),
        #f"{NAMESPACE_COMMON_MODEL}:senderServiceUri": bndl.valid_qualified_name(f'{BACKWARD_PROVN_NAMESPACE}'),
        f"{NAMESPACE_COMMON_MODEL}:metabundle": bndl.valid_qualified_name(f'{BACKWARD_PROVN_NAMESPACE}:{BUNDLE_META}')
    })
    bndl.wasAttributedTo(conn_sample_connector, send_agent)
    
    # Receipt Activity
    act_receipt = bndl.activity(f"receipt", other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:receiptActivity"),
    })
    bndl.used(act_receipt, conn_sample_connector)
    bndl.wasInvalidatedBy(conn_sample_connector, act_receipt)
    
    # Current connector
    entity_identifier = 'sample'
    conn_sample = bndl.entity(bndl.valid_qualified_name(f"{DOI_NAMESPACE}:{entity_identifier}"), other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f'{NAMESPACE_COMMON_MODEL}:currentConnector'),
        f"{NAMESPACE_COMMON_MODEL}:currentBundle": bndl.valid_qualified_name(f'{PROVN_NAMESPACE}:{BUNDLE_PATHOLOGY_DEPARTMENT}'),
        f"{NAMESPACE_COMMON_MODEL}:metabundle": bndl.valid_qualified_name(f'{PROVN_NAMESPACE}:{BUNDLE_META}')
    })
    bndl.wasGeneratedBy(conn_sample, act_receipt)
    bndl.wasDerivedFrom(conn_sample, conn_sample_connector)
    
    # Main Acitivity
    act_phys_slide_prep = bndl.activity(f'physicalSlidePreparation', other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:mainActivity")
    })    
    bndl.used(act_phys_slide_prep, conn_sample)
    
    # WSI Data Connectors
    forward_connectors = []
    for entity_identifier, entity_attributes in prov_req.items():
        for prefix_name, prefix_uri in entity_attributes['prefixes'].items():
            doc.add_namespace(prefix_name, prefix_uri)
            bndl.add_namespace(prefix_name, prefix_uri)
        conn_wsi_forward = bndl.entity(bndl.valid_qualified_name(f"{DOI_NAMESPACE}:{entity_identifier}"), other_attributes={
            f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:forwardConnector"),
            f"{NAMESPACE_COMMON_MODEL}:receiverBundleId": bndl.valid_qualified_name(entity_attributes[f"{NAMESPACE_COMMON_MODEL}:receiverBundleId"]),
            #f"{NAMESPACE_COMMON_MODEL}:receiverServiceUri": bndl.valid_qualified_name(entity_attributes[f"{NAMESPACE_COMMON_MODEL}:receiverServiceUri"]),
            f"{NAMESPACE_COMMON_MODEL}:metabundle": bndl.valid_qualified_name(entity_attributes[f"{NAMESPACE_COMMON_MODEL}:metabundle"]),
        })
        forward_connectors.append(conn_wsi_forward)
        bndl.wasGeneratedBy(conn_wsi_forward, act_phys_slide_prep)
        bndl.wasDerivedFrom(conn_wsi_forward, conn_sample)
    
    # Sender Agent
    rec_agent = bndl.agent(f"researchDataCentre", other_attributes={
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_COMMON_MODEL}:receiverAgent")
    })
    for conn_wsi_forward in forward_connectors:
        bndl.wasAttributedTo(conn_wsi_forward, rec_agent)
    
    # --------------------------------------------------------------------------------------------------------
    data = parse_log(nis_log)
    
    # Annotated WSI Entity
    entity_identifier = 'annotatedWSI'
    ent_annot_wsi = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={})
    for conn_wsi_forward in forward_connectors:
        bndl.specializationOf(ent_annot_wsi, conn_wsi_forward)
    
    # Dataset Creation Activity
    act_dataset_create = bndl.activity(f'datasetCreation', other_attributes={})
    act_phys_slide_prep.add_attributes({f'{NAMESPACE_DCT}:hasPart': 'datasetCreation'})
    bndl.wasGeneratedBy(ent_annot_wsi, act_dataset_create)
    
    # Annotated WSI 2 Entity
    entity_identifier = 'annotatedWSI_2'
    ent_annot_wsi_2 = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={})
    for slide_name, scanner_name in data['ScannerId'].items():
        ent_annot_wsi_2.add_attributes({slide_name: scanner_name})
    bndl.wasDerivedFrom(ent_annot_wsi, ent_annot_wsi_2)
    bndl.used(act_dataset_create, ent_annot_wsi_2)
    
    # Examination Activity
    act_examine = bndl.activity(f'examination', other_attributes={})
    act_phys_slide_prep.add_attributes({f'{NAMESPACE_DCT}:hasPart': 'examination'})
    bndl.wasGeneratedBy(ent_annot_wsi_2, act_examine)
    
    # Scan Entity
    glass_to_scan = {}
    for ent_scan_idx, slide_name in enumerate(data['ScannerId']):
        entity_identifier = f'scan_{ent_scan_idx}'
        ent_scan_i = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={
            "scanId": slide_name
        })
        bndl.used(act_examine, ent_scan_i)
        bndl.wasDerivedFrom(ent_annot_wsi_2, ent_scan_i)
        glass_to_scan[f"{slide_name.replace('_','/')}"] = ent_scan_i
    
    # Scanning Activity
    act_scan = bndl.activity(f'scanning', other_attributes={})
    act_phys_slide_prep.add_attributes({f'{NAMESPACE_DCT}:hasPart': 'scanning'})
    for ent_scan_i in glass_to_scan.values():
        bndl.wasGeneratedBy(ent_scan_i, act_scan)
    
    # Diagnosis Entity
    entity_identifier = 'diagnosis'
    ent_diagnosis = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={
        "diagnosis": f"{data['Diagnosis']}"
    })
    
    # Diagnostics Activity
    act_diagnostics = bndl.activity(f'diagnostics', other_attributes={
        "endTime": f"{data['DiagnosisTime']}",
        f"{NAMESPACE_COMMON_MODEL}:sop": "histologicka_vysetreni_tkani_pf.docx"
    })
    act_phys_slide_prep.add_attributes({f'{NAMESPACE_DCT}:hasPart': 'diagnostics'})
    bndl.wasGeneratedBy(ent_diagnosis, act_diagnostics)
    
    # Stained Glass Entity (from glass_to_scan)
    block_to_glass = defaultdict(list)
    for stained_glass_idx, glass_id in enumerate(glass_to_scan):
        block_id, _ = glass_id.rsplit('-', 1)
        entity_identifier = f'stainedGlass_{stained_glass_idx}'
        ent_stain_glass_i = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={
            "biopticAppId": block_id.split('-')[0],
            "glassId": glass_id
        })
        bndl.used(act_scan, ent_stain_glass_i)
        bndl.wasDerivedFrom(glass_to_scan[glass_id], ent_stain_glass_i)
        bndl.wasDerivedFrom(ent_diagnosis, ent_stain_glass_i)
        bndl.used(act_diagnostics, ent_stain_glass_i)
        block_to_glass[block_id].append(ent_stain_glass_i)
        
    # Doctor Agent
    doc_agent = bndl.agent(f"doctor", other_attributes={
        "employeeId": f"{data['PathologistId']}",
        f"{NAMESPACE_PROV}:type": bndl.valid_qualified_name(f"{NAMESPACE_PROV}:Person"),
    })
    bndl.wasAssociatedWith(act_diagnostics, doc_agent)
    
    # FFPE Activity
    act_ffpe = bndl.activity(f'FFPESlicingStaining', other_attributes={
        "endTime": f"{data['DiagnosisTime']}",
        f"{NAMESPACE_COMMON_MODEL}:sop": "histologicka_vysetreni_tkani_pf.docx"
    })
    act_phys_slide_prep.add_attributes({f'{NAMESPACE_DCT}:hasPart': 'FFPESlicingStaining'})
    for glass_list in block_to_glass.values():
        for ent_stain_glass_i in glass_list:
            bndl.wasGeneratedBy(ent_stain_glass_i, act_ffpe)
        
    # Block Entity (from block_to_glass)
    sample_to_block = defaultdict(list)
    for block_idx, block_id in enumerate(block_to_glass):
        bioptic_id, _ = block_id.split('-')
        entity_identifier = f"block_{block_idx}"
        ent_block_i = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={
            "biopticAppId": bioptic_id,
            "blockId": block_id
        })
        for ent_stain_glass_i in block_to_glass[block_id]:
            bndl.wasDerivedFrom(ent_stain_glass_i, ent_block_i)
        bndl.used(act_ffpe, ent_block_i)
        sample_to_block[bioptic_id].append(ent_block_i)
    
    # Macroscopy Activity
    act_macro = bndl.activity(f'macroscopyBlocksCreation', other_attributes={
        f"{NAMESPACE_COMMON_MODEL}:sop": "histologicka_vysetreni_tkani_pf.docx"
    })
    act_phys_slide_prep.add_attributes({f'{NAMESPACE_DCT}:hasPart': 'macroscopyBlocksCreation'})
    for block_list in sample_to_block.values():
        for ent_block_i in block_list:
            bndl.wasGeneratedBy(ent_block_i, act_macro)
    
    # Sample Block Entity
    ent_sample_all = []
    for sample_idx, sample_id in enumerate(sample_to_block):
        entity_identifier = f'sample_{sample_idx}'
        ent_sample_i = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={
            "biopticAppId": f"{sample_id}"
        })
        bndl.used(act_macro, ent_sample_i)
        for ent_block_i in sample_to_block[sample_id]:
            bndl.wasDerivedFrom(ent_block_i, ent_sample_i)
        bndl.specializationOf(ent_sample_i, conn_sample)
        ent_sample_all.append(ent_sample_i)
    
    # Receipt ID Assignment Activity
    act_receipt_id_assign = bndl.activity(f'receiptIDAssignment', other_attributes={
        f"{NAMESPACE_COMMON_MODEL}:sop": "histologicka_vysetreni_tkani_pf.docx",
        "endTime": f"{data['ReceiptDate']}"
    })
    for ent_sample_i in ent_sample_all:
        bndl.wasGeneratedBy(ent_sample_i, act_receipt_id_assign)
    
    # Receiver Sample Entity
    entity_identifier = 'receiverSample'
    ent_sample_receiver = bndl.entity(bndl.valid_qualified_name(f"{entity_identifier}"), other_attributes={})
    bndl.used(act_receipt_id_assign, ent_sample_receiver)    
    for ent_sample_i in ent_sample_all:
        bndl.wasDerivedFrom(ent_sample_i, ent_sample_receiver)
    bndl.specializationOf(ent_sample_receiver, conn_sample_connector) # ??? missing label
    return doc


def export(doc, nis_log):
    bndl = list(doc.bundles)[0]
    
    subdirs = ['', 'graph', 'json', 'provn', 'log']
    for subdir in subdirs:
        if not (OUTPUT_DIR / subdir).exists():
            (OUTPUT_DIR / subdir).mkdir(parents=True)

    export_to_image(bndl, (OUTPUT_DIR / 'graph' / BUNDLE_PATHOLOGY_DEPARTMENT).with_suffix('.png'))
    export_to_file(doc, (OUTPUT_DIR / 'json' / BUNDLE_PATHOLOGY_DEPARTMENT).with_suffix('.json'), format='json')
    export_to_file(doc, OUTPUT_DIR / 'provn' / BUNDLE_PATHOLOGY_DEPARTMENT, format='provn')
    
    # Provenance of provenance
    output_log = {
        'git_commit_hash': str(pygit2.Repository('.').revparse_single('HEAD').hex),
        'script': str(__file__),
        'eid': str(uuid.uuid4()),
        'input': {
            'nis_log': str(nis_log.resolve()),
        },
        'output': {
            'local_png': str((OUTPUT_DIR / 'graph' / BUNDLE_PATHOLOGY_DEPARTMENT).with_suffix('.png')),
            'remote_png': str(GRAPH_NAMESPACE_URI + str(Path(BUNDLE_PATHOLOGY_DEPARTMENT).with_suffix('.png'))),
            'local_provn': str(OUTPUT_DIR / 'provn' / BUNDLE_PATHOLOGY_DEPARTMENT),
            'remote_provn': str(PROVN_NAMESPACE_URI + BUNDLE_PATHOLOGY_DEPARTMENT)
        }
    }
    with open(OUTPUT_DIR / 'log' / f'{BUNDLE_PATHOLOGY_DEPARTMENT}.log', 'w') as json_out:
        json.dump(output_log, json_out, indent=3)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    # Required arguments
    parser.add_argument('--nis_log', type=Path, required=True, help='Path to sample acquisition log exported from NIS')
    parser.add_argument('--requests', type=Path, required=False, help='Paths to optional requests from other organisations.')
    args = parser.parse_args()    
    
    doc = export_provenance(args.nis_log, args.requests)
    export(doc, args.nis_log)